# Traefik! setup notes

## Setting up frontend basic auth password

``` bash
apt-get install apache2-utils
htpasswd -nb traefik '4e4zL5&c$!zre9yK*rnX'
```

for `traefik.toml/traefik.yaml`
``` bash
traefik:$apr1$H98GCGC5$2sv46rwc6EZ1LFYt7i13M0
```

for `docker-compose.yml` mask all $ by $
``` bash
traefik:$$apr1$$H98GCGC5$$2sv46rwc6EZ1LFYt7i13M0
```

## Creating network
``` bash
docker network create trfk-pub
```

## Clean acme.json before dcocker-compose